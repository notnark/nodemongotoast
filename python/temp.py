# -*- coding: utf-8 -*-
""" Arduino & MongoDB """

import serial
import time
import pymongo
import datetime
import db_settings

from db_settings import db_host, db_user, db_password

T = serial.Serial('/dev/tty.usbmodem1411', 9600)
time.sleep(1)
T.flush()

while True:
    arduino_temp_input = T.readline()
    from pymongo import Connection
    connection = Connection(db_host, 27017)
    db = connection.nodetest1
    now = datetime.datetime.now()
    time = now.strftime("%d/%m %H:%M")
    new_temp = {    "temp": arduino_temp_input,
                    "date": time}
    db.tempcollection.insert(new_temp)
    connection.close()
