/*  Nark.fr - Copyright 2014            */

/*  $Home                               */
/*  $Hello World                        */
/*  $Toast EJS                          */
/*  __________________________________  */


/*  $Home                               */
/*  __________________________________  */
exports.index = function(req, res){
    res.render('index', { title: 'Coucou' });
};


/*  $Hello World                        */
/*  __________________________________  */
exports.helloworld = function(req, res){
    res.render('helloworld', { title: 'Hello, World!'});
};


/*  $Toast EJS                          */
/*  __________________________________  */
exports.toastejs = function(req,res){
    res.render('toastejs.ejs');
};
