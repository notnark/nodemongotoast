/*  Nark.fr - Copyright 2014            */

/*  $Temp List                          */
/*  __________________________________  */


/*  $Temp List                          */
/*  __________________________________  */
exports.templist = function(db) {
    return function(req, res) {
        var collection = db.get('tempcollection');
        collection.find({},{},function(e,docs){

            // Create data array
            var temp = [],
                date = [];
            for (i = 0; i < docs.length; i ++) {
                temp.push(docs[i].temp);
                date.push('\"'+docs[i].date+'\"');
            }

            // Render page
            res.render('temp/templist.ejs', {
                data : {
                    temp: temp,
                    date: date
                },
                title: 'Temperature List'
            });
        });
    };
};
