$(function () {
    "use strict";
/*  Import Data                         */
/*  __________________________________  */
    var data = getData();
/*  Draw Chart                          */
/*  __________________________________  */
    var pollChart = new Highcharts.Chart({
        chart: {
           renderTo: 'container',
           type: 'spline'
        },
        title: {
            text: 'Temperature',
            x: -20 //center
        },
        xAxis: {
            title: {
                text: 'Date & Heure'
            },
            categories: data.date
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            },
            min: 0,
            max: 35,
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '°C'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Lille',
            data: data.temp
        }]
    });
});
